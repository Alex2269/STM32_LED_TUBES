#ifndef __tim4_H
#define __tim4_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

void TIM4_reinit(void);

#ifdef __cplusplus
}
#endif
#endif /*__ tim4_H */
