/***********************************************************************
 port from arduino project to avr-gcc
 https://github.com/Play-Zone/TM1638Module
*************************************************************************/

#ifndef tm1638lib_h
#define tm1638lib_h

#include "stdint.h"
#include "pins_ext.h"

void TM1638Lib(int _DIO, int _CLK, int _STB);
void LedDisplay(uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t);
void TM1638_indicate(uint32_t number);
uint8_t ReadKey(void);

void TM1638_Write(uint8_t);
uint8_t TM1638_Read(void);
void Write_COM(uint8_t);
void Write_DATA(uint8_t, uint8_t);
extern int DIO;// = 8;
extern int CLK;// = 9;
extern int STB;// = 10;

#endif
